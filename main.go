package main

import (
	"gitlab.com/mountblue/cohort-13-golang/stack-implementation/stack"

)

func main() {
	stack.Push("a")
	stack.Push("a")
	stack.Pop()
	stack.Pop()
}
